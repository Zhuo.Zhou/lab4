package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{
	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		// Create a 2-dimention array to store the states of next generation
		CellState[][] newState = new CellState[currentGeneration.numRows()][currentGeneration.numColumns()];
		for(int i=0; i<currentGeneration.numRows(); i++) {
			for(int j=0; j<currentGeneration.numColumns(); j++) {
				newState[i][j] = getNextCell(i, j);
			}
		}
		
		// Set states of nextGeneration with newState obtained above
		for(int i=0; i<nextGeneration.numRows(); i++) {
			for(int j=0; j<nextGeneration.numColumns(); j++) {
				nextGeneration.set(i, j, newState[i][j]);
			}
		}
		
	}

	/**
	 * Get the next state of cell at row and col
	 */
	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		if(currentGeneration.get(row, col).equals(CellState.ALIVE)) return CellState.DYING;
		else if(currentGeneration.get(row, col).equals(CellState.DYING)) return CellState.DEAD;
		else {
			if(countNeighbors(row, col, currentGeneration.get(row, col)) == 2) return CellState.ALIVE;
			else return CellState.DEAD;
		}
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		CellState currentState = currentGeneration.get(row, col);
		int aliveNeighbors = 0;
		// loop over all neighbours
		for(int i = row-1; i<= row+1; i++) {
			if(i<0 || i>=currentGeneration.numRows()) continue;  // skip rows out of bounds
			for(int j = col-1; j<= col+1; j++) {
				if(j<0 || j>=currentGeneration.numColumns()) continue;  // skip columns out of bounds
				if(i == row && j == col) continue;  // skip the cell it self
				if(currentGeneration.get(i, j).equals(CellState.ALIVE)) aliveNeighbors++;
			}
		}
		return aliveNeighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}	
}
