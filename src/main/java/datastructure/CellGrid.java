package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

	int rows;
	int columns;
	CellState[][] states;
    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
    	this.rows = rows;
    	this.columns = columns;
    	states = new CellState[rows][columns];
    	for(int i=0; i<rows; i++) {
    		for(int j=0; j<columns; j++) {
    			states[i][j] = initialState;
    		}
    	}
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
    	if(row < 0 || column < 0 || row > numRows() || column > numColumns()) {
    		throw new IndexOutOfBoundsException("row or column invalid, 0 <= row < " + numRows()
    		+ ", 0 <= column < " + numColumns());
    	}
    	states[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
    	if(row < 0 || column < 0 || row > numRows() || column > numColumns()) {
    		throw new IndexOutOfBoundsException("row or column invalid, 0 <= row < " + numRows()
    		+ ", 0 <= column < " + numColumns());
    	}
        return states[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
    	CellGrid gridCopy = new CellGrid(numRows(), numColumns(), CellState.DEAD);
    	for(int i=0; i<numRows(); i++) {
    		for(int j=0; j<numColumns(); j++) {
    			gridCopy.set(i, j, get(i,j));
    		}
    	}
        return gridCopy;
    }
    
}
